const containerElement=document.querySelector(".container");


for(let index=0;index<30;index++){
    const colorContainerElement = document.createElement("div");
    colorContainerElement.classList.add("color-container");
    containerElement.appendChild(colorContainerElement);
}
const colorContainerEls=document.querySelectorAll(".color-container")






const generateColors=()=>{
    colorContainerEls.forEach((colorContainerEl)=>{
        const newColorCode=randomColor();
        colorContainerEl.style.backgroundColor="#"+newColorCode;
        colorContainerEl.innerHTML="#" + newColorCode;
    });
}




generateColors();

function randomColor(){
    const characters="0123456789abcdef";
    const colorCodeLength=6;
    let color="";
    for(let index=0;index<colorCodeLength;index++){
        const random=Math.floor(Math.random()*characters.length);
        color += characters.substring(random,random+1);
         
    }
    return color;
}