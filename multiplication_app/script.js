const num1=Math.ceil(Math.random()*10);
const num2=Math.ceil(Math.random()*10);
const questionEl=document.getElementById("question");
const inputElement=document.getElementById("input");
const formElement=document.getElementById("form");
const scoreElement=document.getElementById("score");

const correctAnswer=num1*num2;
questionEl.innerText=`What is the multiplication of ${num1} and ${num2}`;

let score=JSON.parse(localStorage.getItem("score"));
if(!score){
    score=0;
}
scoreElement.innerText=`Score: ${score}`;

function updateInLocalStorage(){
    localStorage.setItem("score",JSON.stringify(score));
}
formElement.addEventListener("submit",()=>{
    const answer= +inputElement.value;
    if(answer==correctAnswer){
        score++;
        updateInLocalStorage()
    }
    else{
        score--;
        updateInLocalStorage()
    }
    console.log(score);
    

})









