const hourElement=document.getElementById("hours");
const minutesElement=document.getElementById("minutes");
const secondElement=document.getElementById("seconds");
const ampnElement=document.getElementById("ampm");

function updateClock(){
    let hour=new Date().getHours();
    let minutes=new Date().getMinutes();
    let second=new Date().getSeconds();
    let ampm="AM"

    if(hour>12){
        hour=hour-12;
        ampm="PM";
    }
    hour = hour<10 ? "0"+hour: hour;

    hourElement.innerText=hour;
    minutesElement.innerText=minutes;
    secondElement.innerText=second;
    ampnElement.innerText=ampm;

    setTimeout(()=>{
        updateClock();
    },1000);
}
updateClock();