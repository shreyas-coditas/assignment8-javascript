const textareaElement=document.getElementById("textarea");
const totalCharacterElement=document.getElementById("total-counter");
const remainingCharacterElement=document.getElementById("remaining-counter");



textareaElement.addEventListener("keyup",()=>{
    updateCounter();
})

const updateCounter=()=>{
    const totalCharacters=textareaElement.value.length;
    const remaining=textareaElement.getAttribute("maxlength")-totalCharacters;
    console.log(remaining);
    totalCharacterElement.innerText=`${totalCharacters}`;
    remainingCharacterElement.innerText=`${remaining}`;
}